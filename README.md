# Magento MAGMI Sample Data

The CSV file in the magmi_import_data_002.zip can be imported using MAGMI available here: (http://sourceforge.net/apps/mediawiki/magmi/index.php "Sourceforge MAGMI")

Place the CSV file into the [magento-root]/var/import/ directory before import.

The sample product image sample_product_image_001.jpg should be placed in the [magento-root]/media/import/ directory before import. 
